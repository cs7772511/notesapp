﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace NotesApp
{
    public class Notes
    {
        private NoteEntities db;

        public Notes()
        {
            db = new NoteEntities();
        }

        public IEnumerable<Note> GetNotes()
        {
            return
              from n in db.Notes // db.Notes
              orderby n.Name
              select n;
        }

        public Note GetNote(int id)
        {
            var note =
              from n in db.Notes // db.Notes
              where n.Id == id
              select n;

            if (!note.Any())
            {
                return null;
            }

            return note.First();
        }

        public void DeleteNote(Note note)
        {
            var dbNote = db.Notes.Find(note.Id);
            if (dbNote != null)
            {
                db.Notes.Remove(dbNote);
                db.SaveChanges();
            }
        }

        public void UpdateNote(Note note)
        {
            db.Notes.AddOrUpdate(note);
            db.SaveChanges();
        }
    }
}

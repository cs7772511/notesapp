namespace NotesApp
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Notes")]
    public partial class Note
    {
        public Note() { }
        public Note(int id, string name, string content)
        {
            Id = id;
            Name = name;
            Content = content;
        }
        public int? Id { get; set; }

        public string Content { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}


using System.Data.Entity;

namespace NotesApp
{
    public partial class NoteEntities : DbContext
    {
        public NoteEntities()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Note> Notes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
